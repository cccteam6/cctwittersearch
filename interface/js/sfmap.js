/*global console, jQuery, $, _, Backbone */

// Team 23 - San Francisco - Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065), 
// Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142), 
// Rupali Patil (726 452)

// Globals
var server_url = "/";


function initialize() {
    var mapOptions = {
        center: { lat: 37.7577, lng: -122.4376},
        zoom: 12
    };
    var map = new google.maps.Map(document.getElementById('map-canvas'),
                                  mapOptions);
    var styles = [
        {
            stylers: [
                { hue: "#00ffe6" },
                { saturation: -20 }
            ]
        },{
            featureType: "road",
            elementType: "geometry",
            stylers: [
                { lightness: 100 },
                { visibility: "simplified" }
            ]
        },{
            featureType: "road",
            elementType: "labels",
            stylers: [
                { visibility: "off" }
            ]
        }
    ];

    map.setOptions({styles: styles});

    $("#get-all-coords").click(function() {
        $.get(server_url + "geo/all_coordinates", function(data) {
            for (var i = 0; i < data.length; i++) {
                var lat = parseFloat(data[i].key[1]);
                var lon = parseFloat(data[i].key[0]);
                var latlon = new google.maps.LatLng(lat, lon);
                // Add new Marker
                console.log('Adding marker: ' + latlon);
                var marker = new google.maps.Marker({
                    position: latlon,
                    map: map,
                    title: '::'
                });
            }            
        });
    });

    $("#get-user-coords").click(function() {
        var user_name = $("#user-id-input").val();
        // do an api call to get the user id
        $.get(server_url + "user/get_info_by_name/" +
              user_name.toLowerCase(), function(data) {
                  var user_id = data.id;

                  $.get(server_url + "geo/user/" + user_id, function(data) {
                      console.log(data);
                      for (var i = 0; i < data.length; i++) {
                          var lat = parseFloat(data[i].value[1]);
                          var lon = parseFloat(data[i].value[0]);
                          var latlon = new google.maps.LatLng(lat, lon);
                          var marker = new google.maps.Marker({
                              position: latlon,
                              map: map,
                              title: '::'
                          });            
                      }
                      
                  });
            
        });
        
        
    });
    
}

google.maps.event.addDomListener(window, 'load', initialize);


