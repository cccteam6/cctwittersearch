// https://developers.google.com/maps/documentation/javascript/examples/layer-heatmap

// Team 23 - San Francisco - Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065), 
// Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142), 
// Rupali Patil (726 452)

var map, pointarray, heatmap;
/*global console, jQuery, $, _, Backbone */

// Globals
var server_url = "/";
var processedData;

var LAT = 37.774546;
var LNG = -122.433523;


function initialize() {
    
    var mapOptions = {
        zoom: 13,
        center: new google.maps.LatLng(37.774546, -122.433523),
        mapTypeId: google.maps.MapTypeId.SATELLITE
    };

    map = new google.maps.Map(document.getElementById('map-canvas'),
                              mapOptions);

    $.get(server_url + "geo/all_coordinates", function(data) {
        // 10135+ coordinates way out - dilutes heatmap?
        // fix
        processedData = new Array(data.length);
        for (i = 0; i < data.length; i++) {
            var lat = data[i]["key"][1];
            var lng = data[i]["key"][0];
            
            if (Math.abs(lat - LAT) < 0.1 && Math.abs(lng - LNG) < 0.1) {
                
                var coord = new google.maps.LatLng(lat, lng);
                processedData[i] = coord;
            }
        }
        
        pointArray = new google.maps.MVCArray(processedData);

        
        heatmap = new google.maps.visualization.HeatmapLayer({
            data: pointArray
        });
        heatmap.setMap(map);
        changeGradient();
    });     
}

chunk = 0;
multiplier = 500;
sampleArray = new Array();

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function toggleHeatmap() {
    
    if (chunk * (multiplier + 1) >= processedData.length - 1) {
        
        chunk = 0;
        sampleArray = new Array();
        
    } else {
        
        chunk += 1;
        
        for (var i = 0; i < multiplier; i++) {
            var rand = getRandomInt(0, processedData.length - 1);
            var coord = processedData[rand];
            sampleArray.push(coord);
        }
    }
    
    heatmap.setMap(heatmap.getMap() ? null : map);
    pointArray = new google.maps.MVCArray(sampleArray);

    
    heatmap = new google.maps.visualization.HeatmapLayer({
        data: pointArray
    });    
    heatmap.setMap(map);
    changeGradient();
    
    setTimeout("toggleHeatmap()", 3 * 1000);
}



function changeGradient() {
    var gradient = [
        'rgba(0, 255, 255, 0)',
        'rgba(0, 255, 255, 1)',
        'rgba(0, 191, 255, 1)',
        'rgba(0, 127, 255, 1)',
        'rgba(0, 63, 255, 1)',
        'rgba(0, 0, 255, 1)',
        'rgba(0, 0, 223, 1)',
        'rgba(0, 0, 191, 1)',
        'rgba(0, 0, 159, 1)',
        'rgba(0, 0, 127, 1)',
        'rgba(63, 0, 91, 1)',
        'rgba(127, 0, 63, 1)',
        'rgba(191, 0, 31, 1)',
        'rgba(255, 0, 0, 1)'
    ];
    heatmap.set('gradient', gradient);
}

function changeRadius() {
    heatmap.set('radius', heatmap.get('radius') ? null : 20);
}

function changeOpacity() {
    heatmap.set('opacity', heatmap.get('opacity') ? null : 0.2);
}

google.maps.event.addDomListener(window, 'load', initialize);

