/*global console, jQuery, $, _, Backbone */
// Team 23 - San Francisco - Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065), 
// Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142), 
// Rupali Patil (726 452)

// Globals
var server_url = "/";
// var server_url = "http://localhost:8080/";

$(document).ready(function() {
    // Get the topic
    if ($("#get-topic-analysis").length) {
        
        $("#get-topic-analysis").click(function() {
            var btn = $(this).button('loading');
            var topic = $("#topic-input").val();
            var encoded_topic = encodeURIComponent(topic);

            // Get request to get the analysis of the topic
            $.get(server_url + "sent/topic/" + encoded_topic, function(data) {                
                var sent_tup = data.rows[0].sentiments;            
                var total = sent_tup.reduce(function(prev, curr, index, array) {
                    return prev + curr;
                });
                // Calculate proportions
                var pos = Number((sent_tup[1] + sent_tup[0]) / total * 100).toFixed(2);
                var neutral = Number(sent_tup[2] / total * 100).toFixed(2);
                var neg = Number((sent_tup[3] + sent_tup[4]) / total * 100).toFixed(2);
                

                // print to interface
                $("#total-count").text(total);
                $("#extreme-positive-count").text(pos + "%");
                $("#neutral-count").text(neutral + "%");
                $("#extreme-negative-count").text(neg + "%");

                $("#get-topic-analysis").button('reset');
            });
            
        });
    }

    // Get tweets page
    if ($("#get-tweets").length) {
        
        $("#get-tweets").click(function() {
            var btn = $(this).button('loading');
            var topic = $("#topic-input").val();
            var encoded_topic = encodeURIComponent(topic);

            // Get request to get the analysis of the topic
            $.get(server_url + "tweets/topic/" + encoded_topic, function(data) {
                // data is an array of arrays
                for (var i = 0; i < data.length; i++) {
                    for (var j = 0; j < data[i].length; j++) {
                        $("#tweets-table > tbody").append('<tr><td>' +
                                                          data[i][j] +
                                                          '</td></tr>');                                               
                    }
                }

                $("#get-tweets").button('reset');
            });
        });
    }

    ////////////////////////////////////////////////////////////////////////
    // GET requests for the trends page                                   //
    ////////////////////////////////////////////////////////////////////////

    // Week counts
    $("#get-topic-week-count").on("click", function() {
        // Show loading
        var btn = $(this).button('loading');
        var topic = $("#topic-input").val();
        var encoded_topic = encodeURIComponent(topic);
        $.get(server_url + "sent/weekcount/" + encoded_topic, function(data) {
            var rows = data.rows[0].week_counts;            
            var dataobj = [];
            var days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
            for (var i = 0; i < rows.length; i++) {
                var obj = {day_name: days[i], day_count: rows[i], index: i + 1};               
                dataobj.push(obj);
            }
            console.log(dataobj);
            // morris line graph
            Morris.Area({
                element: 'week-count-graph',
                data: dataobj,
                xkey: 'index',
                ykeys: ['day_count'],
                labels: ['Count'],
                parseTime: false,
                xLabelFormat: function(x) { return days[x.x];}                
            });
            $("#get-topic-week-count").button('reset');
        });
        
    });
    
    

    ////////////////////////////////////////////////////////////////////////
    // // GET requests for scenarios page                                 //
    ////////////////////////////////////////////////////////////////////////
    // check if donut row is in view
    if ($("#donut-row").length) {

        ////////////////////////////////////////////////////////////////////
        // Request for loading the sports preset topics data              //
        ////////////////////////////////////////////////////////////////////
        $.get(server_url + "sent/preset/sports", function(data) {

            // display the loaded topics
            $("#topics-rendered").text("Topics covered: " + data.topics);

            var rows = data.rows;
            for (var i = 0; i < rows.length; i++) {
                var key = "sentiment_" + data.topics[i];
                var row = rows[i];
                // Get the sentiment list for the current topic
                var sent_tup = row[key];

                // Append a div to the donut row to create the morris donut in
                // id of the new div
                var divid = "sports_donut_" + i;
                var outerdiv = $("<div></div>").attr('class', 'col-md-4');
                var div = $("<div></div>")                        
                        .attr('class', 'panel panel-success');
                // Panel heading
                div.append($("<div></div>")
                           .attr('class', 'panel-heading')
                           .text(data.topics[i]));
                // Panel Body
                div.append($("<div></div>")
                           .attr('class', 'panel-body')
                           .attr('id', divid)
                           .height(200));
                outerdiv.append(div);
                $("#donut-row").append(outerdiv);

                // Create a morris donut
                Morris.Donut({
                    element: divid,
                    data: [
                        {label: "Positive", value: sent_tup[0] + sent_tup[1]},
                        {label: "Neutral", value: sent_tup[2]},
                        {label: "Negative", value: sent_tup[3] + sent_tup[4]}
                    ],
                    colors: ['#336699', 'gray', '#A62A2A' ]
                });
            }
        });

        ////////////////////////////////////////////////////////////////////
        // Donuts for tourists                                            //
        ////////////////////////////////////////////////////////////////////
        $.get(server_url + "sent/preset/tourist_places", function(data) {

            // display the loaded topics
            $("#topics-rendered-tourist").text("Topics covered: " + data.topics);

            var rows = data.rows;
            for (var i = 0; i < rows.length; i++) {
                var key = "sentiment_" + data.topics[i];
                var row = rows[i];
                // Get the sentiment list for the current topic
                var sent_tup = row[key];
                
                // Append a div to the donut row to create the morris donut in
                // id of the new div
                var divid = "tourist_donut_" + i;
                var outerdiv = $("<div></div>").attr('class', 'col-md-4');
                var div = $("<div></div>")                        
                        .attr('class', 'panel panel-success');
                // Panel heading
                div.append($("<div></div>")
                           .attr('class', 'panel-heading')
                           .text(data.topics[i]));
                // Panel Body
                div.append($("<div></div>")
                           .attr('class', 'panel-body')
                           .attr('id', divid)
                           .height(200));
                outerdiv.append(div);
                $("#donut-row-tourist").append(outerdiv);

                // Create a morris donut
                Morris.Donut({
                    element: divid,
                    data: [
                        {label: "Positive", value: sent_tup[0] + sent_tup[1]},
                        {label: "Neutral", value: sent_tup[2]},
                        {label: "Negative", value: sent_tup[3] + sent_tup[4]}
                    ],
                    colors: ['#336699', 'gray', '#A62A2A' ]
                });
            }
        });

        ////////////////////////////////////////////////////////////////////
        // Donuts for transport                                           //
        ////////////////////////////////////////////////////////////////////
        $.get(server_url + "sent/preset/transport", function(data) {

            // display the loaded topics
            $("#topics-rendered-transport").text("Topics covered: " + data.topics);

            var rows = data.rows;
            for (var i = 0; i < rows.length; i++) {
                var key = "sentiment_" + data.topics[i];
                var row = rows[i];
                // Get the sentiment list for the current topic
                var sent_tup = row[key];
                
                // Append a div to the donut row to create the morris donut in
                // id of the new div
                var divid = "transport_donut_" + i;
                var outerdiv = $("<div></div>").attr('class', 'col-md-4');
                var div = $("<div></div>")                        
                        .attr('class', 'panel panel-success');
                // Panel heading
                div.append($("<div></div>")
                           .attr('class', 'panel-heading')
                           .text(data.topics[i]));
                // Panel Body
                div.append($("<div></div>")
                           .attr('class', 'panel-body')
                           .attr('id', divid)
                           .height(200));
                outerdiv.append(div);
                $("#donut-row-transport").append(outerdiv);

                // Create a morris donut
                Morris.Donut({
                    element: divid,
                    data: [
                        {label: "Positive", value: sent_tup[0] + sent_tup[1]},
                        {label: "Neutral", value: sent_tup[2]},
                        {label: "Negative", value: sent_tup[3] + sent_tup[4]}
                    ],
                    colors: ['#336699', 'gray', '#A62A2A' ]
                });
            }
        });
        
    }
});
