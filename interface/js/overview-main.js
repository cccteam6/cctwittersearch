/*global console, jQuery, $, _, Backbone, Morris, location */
// Team 23 - San Francisco - Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065), 
// Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142), 
// Rupali Patil (726 452)

// Globals
var server_url = "/";
// var server_url = "http://localhost:8080/";

// Manually loading the result of GET help/languages from twitter search API
var language_codes = [
    {
        "code": "fr",
        "name": "French",
        "status": "production"
    },
    {
        "code": "en",
        "name": "English",
        "status": "production"
    },
    {
        "code": "ar",
        "name": "Arabic",
        "status": "production"
    },
    {
        "code": "ja",
        "name": "Japanese",
        "status": "production"
    },
    {
        "code": "es",
        "name": "Spanish",
        "status": "production"
    },
    {
        "code": "de",
        "name": "German",
        "status": "production"
    },
    {
        "code": "it",
        "name": "Italian",
        "status": "production"
    },
    {
        "code": "id",
        "name": "Indonesian",
        "status": "production"
    },
    {
        "code": "pt",
        "name": "Portuguese",
        "status": "production"
    },
    {
        "code": "ko",
        "name": "Korean",
        "status": "production"
    },
    {
        "code": "tr",
        "name": "Turkish",
        "status": "production"
    },
    {
        "code": "ru",
        "name": "Russian",
        "status": "production"
    },
    {
        "code": "nl",
        "name": "Dutch",
        "status": "production"
    },
    {
        "code": "fil",
        "name": "Filipino",
        "status": "production"
    },
    {
        "code": "msa",
        "name": "Malay",
        "status": "production"
    },
    {
        "code": "zh-tw",
        "name": "Traditional Chinese",
        "status": "production"
    },
    {
        "code": "zh-cn",
        "name": "Simplified Chinese",
        "status": "production"
    },
    {
        "code": "hi",
        "name": "Hindi",
        "status": "production"
    },
    {
        "code": "no",
        "name": "Norwegian",
        "status": "production"
    },
    {
        "code": "sv",
        "name": "Swedish",
        "status": "production"
    },
    {
        "code": "fi",
        "name": "Finnish",
        "status": "production"
    },
    {
        "code": "da",
        "name": "Danish",
        "status": "production"
    },
    {
        "code": "pl",
        "name": "Polish",
        "status": "production"
    },
    {
        "code": "hu",
        "name": "Hungarian",
        "status": "production"
    },
    {
        "code": "fa",
        "name": "Persian",
        "status": "production"
    },
    {
        "code": "he",
        "name": "Hebrew",
        "status": "production"
    },
    {
        "code": "th",
        "name": "Thai",
        "status": "production"
    },
    {
        "code": "uk",
        "name": "Ukrainian",
        "status": "production"
    },
    {
        "code": "cs",
        "name": "Czech",
        "status": "production"
    },
    {
        "code": "ro",
        "name": "Romanian",
        "status": "production"
    },
    {
        "code": "en-gb",
        "name": "British English",
        "status": "production"
    },
    {
        "code": "vi",
        "name": "Vietnamese",
        "status": "production"
    },
    {
        "code": "bn",
        "name": "Bengali",
        "status": "production"
    }
];

// getNameForCode: returns the languages name for the given language code
function getNameForCode(lang_code) {    
    for (var i = 0; i < language_codes.length; i++) {
        var lang = language_codes[i];
        if (lang.code == lang_code) {
            return lang.name;
        }
    }
    return null;
}

$(document).ready(function() {

    if ($('#language-bar-chart').length) {
        $.get(server_url + "tweets/top_languages", function(data) {        
            var total = 0;   // Sum for percentage proportions
            var total_exc = 0;  // Sum excluding counting english

            // Get the total language count
            for (var i = 0; i < data.length; i++) {
                var language = data[i];                        
                total += language.value;
                if (language.key != "en")
                    total_exc += language.value;
            }

            // Convert all the language counts into percentages
            // Also create a sepearate proportion for non en languages
            data.map(function(obj) {
                // Read the language code
                var lang_name = getNameForCode(obj.key);
                if (lang_name) {
                    obj['name'] = lang_name;
                } else {
                    // If name was not found, better to use the key
                    obj['name'] = obj.key;
                }
                obj['proportion'] = obj.value / total * 100;
                if (obj.key != "en")
                    obj['non_english_prop'] = obj.value / total_exc * 100;
            });

            // Filter out the small insignificant percentages
            var filtered_data = data.filter(function(obj) {
                if (obj.proportion < 1 || obj.key == "und")
                    return false;
                return true;
            });

            // Filter for removing english counts
            var non_english_data = data.filter(function(obj) {
                if (obj.non_english_prop < 1 || obj.key == "und" || obj.key == "en")
                    return false;
                return true;
            });
            
            Morris.Bar({
                element: 'language-bar-chart',
                data: filtered_data,
                xkey: 'name',
                ykeys: ['proportion'],
                labels: ['Language percentage']            
            });

            Morris.Bar({
                element: 'language-bar-chart-without-eng',
                data: non_english_data,
                xkey: 'name',
                ykeys: ['non_english_prop'],
                labels: ['Language percentage']            
            });
            
        });
    }


    ////////////////////////////////////////////////////////////////////////
    // Language Sentiments                                                //
    ////////////////////////////////////////////////////////////////////////
    // check if donut row is in view
    if ($("#language-donut-row").length) {

        ////////////////////////////////////////////////////////////////////
        // Request for loading the sports preset topics data              //
        ////////////////////////////////////////////////////////////////////
        $.get(server_url + "sent/languages_sentiments", function(data) {
            $("#loading-thing").text("Loading Donuts...");
            var rows = data.rows;                        
            for (var i = 0; i < rows.length; i++) {
                console.log(rows[i]);
                var sent_obj = rows[i];
                var label = "";
                var sent_tup = [];
                // Get the object name
                if ("sentiments_en" in sent_obj) {                    
                    label = "English";
                    sent_tup = sent_obj['sentiments_en'];
                }
                else if ("sentiments_it" in sent_obj) {
                    label = "Italian";
                    sent_tup = sent_obj['sentiments_it'];
                }                
                else {
                    label = "Spanish";
                    sent_tup = sent_obj['sentiments_es'];
                }

                // Append a div to the donut row to create the morris donut in
                // id of the new div
                var divid = label;
                var outerdiv = $("<div></div>").attr('class', 'col-md-4');
                var div = $("<div></div>")                        
                        .attr('class', 'panel panel-success');
                // Panel heading
                div.append($("<div></div>")
                           .attr('class', 'panel-heading')
                           .text(label));
                // Panel Body
                div.append($("<div></div>")
                           .attr('class', 'panel-body')
                           .attr('id', divid)
                           .height(200));
                outerdiv.append(div);
                $("#language-donut-row").append(outerdiv);
                console.log(sent_tup);
                // Create a morris donut
                Morris.Donut({
                    element: divid,
                    data: [
                        {label: "Positive", value: sent_tup[0] + sent_tup[1]},         
                        {label: "Negative", value: sent_tup[3] + sent_tup[4]}
                    ],
                    colors: ['#336699', '#A62A2A' ]
                });
            }
            $("#loading-thing").text("");
        });
    }
    
    
    // Fill the retweet table
    if ($("#retweet-table").length) {        
        $.get(server_url + "tweets/top_retweets", function(data) {            
            // Empty the #retweet-table
            console.log(data);
            $("#retweet-table > tbody").html('');
            var rows = data.rows;
            for (var i = 0; i < rows.length; i++) {
                var tweet_text = rows[i].text;
                var tweet_count = rows[i].value;
                $("#retweet-table > tbody").append('<tr><td>' +
                                                   tweet_text +
                                                   '</td><td>' +
                                                   tweet_count +
                                                   '</td></tr>');
            }
        });
    }

    ////////////////////////////////////////////////////////////////////////
    // Main overview page                                                 //
    ////////////////////////////////////////////////////////////////////////

    // filling the top followed table
    if ($("#top-followed-table").length) {
        $("#most-followed-user").text('Loading...');
        $.get(server_url + "users/most_followed", function(data) {
            // Show loading
            
            console.log(data);
            // empty the table
            $("#top-followed-table > tbody").html('');
            for (var i = 0; i < data.length; i++) {
                var user_name = data[i].key;
                var follow_count = data[i].value;
                $("#top-followed-table > tbody").append('<tr><td>' +
                                                        user_name +
                                                        '</td><td>' +
                                                        follow_count + " followers" +
                                                        '</td></tr>');
            }

            // Also set the most followed panel
            $("#most-followed-user").text(data[0].key);
            $("#most-followed-user-count").text(data[0].value);
            
        });        
    }

    // filling the top tweeters
    if ($("#top-tweeter-table").length) {
        $("#top-tweeter").text('Loading...');
        $.get(server_url + "users/top_tweeter", function(data) {
            // Show loading
            
            console.log(data);
            // empty the table
            $("#top-tweeter-table > tbody").html('');
            for (var i = 0; i < data.length; i++) {
                var user_id = data[i].key;
                var tweet_count = data[i].value;
                $("#top-tweeter-table > tbody").append('<tr><td>' +
                                                       user_id +
                                                       '</td><td>' +
                                                       tweet_count + " tweets" +
                                                       '</td></tr>');
            }

            // Also set the most followed panel
            $("#top-tweeter").text(data[0].key);
            $("#top-tweeter-count").text(data[0].value);
            
        });        
    }

    // filling the top mentioned users
    if ($("#top-mentioned-table").length) {
        $("#top-mentioned-user").text('Loading...');
        $.get(server_url + "users/top_tweeter", function(data) {
            // Show loading
            
            console.log(data);
            // empty the table
            $("#top-mentioned-table > tbody").html('');
            for (var i = 0; i < data.length; i++) {
                var user_id = data[i].key;
                var tweet_count = data[i].value;
                $("#top-mentioned-table > tbody").append('<tr><td>' +
                                                         user_id +
                                                         '</td><td>' +
                                                         tweet_count + " tweets" +
                                                         '</td></tr>');
            }
            // Also set the most followed panel
            $("#top-mentioned-user").text(data[0].key);
            $("#top-mentioned-count").text(data[0].value);
            
        });        
    }


    
});

