# Scripts

###All relevant automation scripts are located in the deploy directory, and must be executed from there

    $ cd deploy

# Deploy All

###To install, create environment, and start all services on a list of servers in ip_addresses

    $ ./deploy_all.sh

# Remote Install

###To install, create environment, and start all services on a remote IP

    $ ./remote_install.sh <IP_ADDRESS>
    
# To execute any command on a remote

###This will execute any of the below commands remotely

    $ ./remote.sh <COMMAND> <IP_ADDRESS>    

# One Up

###To install, create environment, and start all services

    $ source oneup.sh

# Setup

###Local Installation

    $ source install.sh
    
### Setup Local Environment

	$ source environment.sh

# Database
	
### Run Database Locally

	$ ./start_database.sh

### Create Local Database

	$ ./create_database.sh
	
### Delete Local Database

	$ ./delete_database.sh		
	
### Reset Local Database

	$ ./reset_database.sh	
	
# Harvester

### Run Harvester Locally

	$ ./start_harvester.sh

# Server
	
### Run Server Locally

	$ ./start_server.sh

# Shutdown/Clear
	
### Stop All Locally

	$ ./stop_all.sh	
	
### Stop All Locally

	$ ./stop_all.sh	

### Stop database

	$ ./stop_database.sh			

### Stop harvester

	$ ./stop_harvester.sh			
	
### Stop server

	$ ./stop_server.sh			
	
### Clear Logs

	$ ./clear_logs.sh
