#!/bin/bash
# Team 23 - San Francisco - Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065), 
# Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142), 
# Rupali Patil (726 452)

source log.sh

while read p; do
  log "Checking on server: $p"
   curl $p
   curl $p:5984
done <ip_addresses
