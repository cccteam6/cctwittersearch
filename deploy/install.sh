#!/bin/sh
# Team 23 - San Francisco - Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065), 
# Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142), 
# Rupali Patil (726 452)

source log.sh

#log "Updating apt"
#sudo apt-get update

log "Installing couchdb"
sudo apt-get -y install couchdb

log "Installing nginx"
sudo apt-get -y install nginx
sudo cp -r nginx /etc/

log "Installing python prereqs"
sudo apt-get -y install python-dev libffi-dev libssl-dev
sudo apt-get -y install python-pip
sudo pip install virtualenv

#chown -R ubuntu /home/ubuntu/.cache/pip

log "Creating and activating virtualenv"
virtualenv venv
source venv/bin/activate

log "Creating log directory"
mkdir ../log/

log "Installing pip requirements"
pip -q install -r requirements.txt
