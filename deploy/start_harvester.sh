#!/bin/bash
# Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065), 
# Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142), 
# Rupali Patil (726 452)

source log.sh

source environment.sh

log "Starting harvester in the background"
nohup sh -c "python ../search/process.py < processor_ip_addresses" > ../log/harvester.out 2> ../log/harvester.err < /dev/null &
