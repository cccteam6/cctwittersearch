#!/bin/bash
# Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065), 
# Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142), 
# Rupali Patil (726 452)

../keys/./auth.sh
ssh -n ubuntu@$2 "cd ~/cctwittersearch/deploy/; source environment.sh;source $1"

