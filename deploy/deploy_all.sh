#!/bin/bash
# Team 23 - San Francisco - Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065), 
# Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142), 
# Rupali Patil (726 452)

source log.sh

while read p; do
  log "Installing on server: $p"
  #log "Logging output to: $p.out"
  #log "Logging errors to: $p.err"
  #./remote_install.sh $p > ../log/$p.out 2> ../log/$p.err < /dev/null
  ./remote_install.sh $p
done <ip_addresses

while read p; do
  log "Setting up harvesters: $p"
	./remote.sh start_harvester.sh $p
done <harvester_ip_addresses
