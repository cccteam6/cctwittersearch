#!/bin/bash
# Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065), 
# Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142), 
# Rupali Patil (726 452)

source log.sh

log "Starting nginx"
sudo service nginx start

source environment.sh

log "Starting gunicorn tweetserver"
cd ../search/
gunicorn -c gunicornconf.py tweetserver:app
