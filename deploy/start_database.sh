#!/bin/bash
# Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065), 
# Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142), 
# Rupali Patil (726 452)

source log.sh

./stop_database.sh

log "Making public"
sudo sed -i -- 's/;bind_address = 127.0.0.1/bind_address = 0.0.0.0/g' /etc/couchdb/local.ini

log "Starting database"
sudo service couchdb start
