#!/bin/bash
# Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065), 
# Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142), 
# Rupali Patil (726 452)

source log.sh

source environment.sh

./stop_all.sh

./start_database.sh

sleep 4

./create_database.sh

sleep 2

log "Starting server in background"
nohup ./start_server.sh > ../log/server.out 2> ../log/server.err < /dev/null &
