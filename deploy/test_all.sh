#!/bin/bash
# Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065), 
# Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142), 
# Rupali Patil (726 452)

source install.sh
source environment.sh
deactivate

./start_database.sh

./create_database.sh         
./update_database.sh
./reset_database.sh
./delete_database.sh                       

./stop_database.sh

./start_server.sh
./stop_server.sh
deactivate

./start_harvester.sh
./stop_harvester.sh
deactivate

./clear_logs.sh

./start_all.sh
./stop_all.sh
deactivate

source oneup.sh
deactivate
