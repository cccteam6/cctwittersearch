#!/bin/bash
# Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065), 
# Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142), 
# Rupali Patil (726 452)

../keys/./auth.sh
rsync -rtvpl ../../cctwittersearch ubuntu@$1:~/ --exclude='venv' --exclude='*.pyc' --exclude='log' 
