#!/bin/bash
# Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065), 
# Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142), 
# Rupali Patil (726 452)

source log.sh

log "Stopping gunicorn tweetserver"
pkill -f tweetserver

log "Stopping nginx"
sudo service nginx stop
