import boto
from boto.ec2.regioninfo import RegionInfo

EC2_ACCESS_KEY = "953f49ddc1724bd9b2279691025f0ed9"
EC2_SECRET_KEY = "b4e63f9e16f24e9cac7cd9dc95e4ffc4"


region = RegionInfo(name = "melbourne", endpoint = "nova.rc.nectar.org.au")
endpoint = "https://nova.rc.nectar.org.au:8773/services/Cloud"
# conn = boto.connect_ec2_endpoint(endpoint ,aws_access_key_id=EC2_ACCESS_KEY,
#                                  aws_secret_access_key=EC2_SECRET_KEY)


conn = boto.connect_ec2(aws_access_key_id = EC2_ACCESS_KEY,
                            aws_secret_access_key = EC2_SECRET_KEY,
                            is_secure = True,
                            region = region,
                            port = 8773,
                            path = "/services/Cloud", validate_certs = True)


images = conn.get_all_images()

for img in images:
    print 'id: ', img.id, 'name: ', img.name
