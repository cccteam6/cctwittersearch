#!/bin/bash
# Team 23 - San Francisco - Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065), 
# Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142), 
# Rupali Patil (726 452)

chmod 400 ../keys/team23_all_access.pem
ssh-add ../keys/team23_all_access.pem
