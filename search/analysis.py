#!/usr/local/bin/python

# Team 23 - San Francisco - Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065), 
# Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142), 
# Rupali Patil (726 452)from tweetstore import TweetStore

from tweetstore import TweetStore
from apiglobals import DBNAME
import datetime
import time
from time import mktime
from textblob import TextBlob
from collections import defaultdict

trends = defaultdict(int)
handles = defaultdict(int)


class Analyser:
    """Sentiment and topic analysis methods"""

    def __init__(self, dbname=DBNAME):
        self.store = TweetStore(dbname)

    def sentiments_of_topic(self, topic):
        """Compute the sentiment tuple of a topic.

        Tweets containing 'topic' are gathered and a five tuple
        sentiment value is calculated. Each of these five values
        count the number of tweets categorised as that sentiment.
        From Extremely negative to Extremely Positive.
        """
        tweets = self.get_tweets_by_topics(topic)
        if len(tweets) > 1:
            return {'rows': [{'sentiments':
                              list(self.analyse_sentiments(tweets))}]}
        else:
            return {'rows': [{'sentiments': [0, 0, 0, 0, 0]}]}

    def get_tweets_by_topics(self, keyword):

        topic_tweets = []
        results = self.store.call_view("_design/tweets",
                                       "by_topics",
                                       options={'key': '"%s"' % keyword})
        if results:
            for row in results['rows']:
                tweets = row['value']
                topic_tweets.append(tweets)
        return topic_tweets

    def _make_sentiment_key(self, topic):
        return "sentiment_%s" % topic

    def multiple_sentiments(self, topics):
        """Get multitude of sentiments on topics."""
        sentiment_list = []
        for topic in topics:
            tweets = self.get_tweets_by_topics(topic)
            sent = self.analyse_sentiments(tweets)
            # Key to use to store in the dictionaru
            sent_key = self._make_sentiment_key(topic)
            sentiment_list.append({sent_key: list(sent)})
        return {'rows': sentiment_list}

    def count_trends_handles(self):
        """count top trending topics and popular user handles.
           Input : None.
           Returns :None.(changes the globals trends and topics) """
        global trends
        global handles
        results = self.store.call_view("_design/tweets",
                                       "by_topics")

        if results:
            for row in results['rows']:
                terms = row['key']
                if (terms.startswith('#')):
                    trends[terms]+=1
                if (terms.startswith('@')):
                    handles[terms]+=1
        trends=sorted(trends,key=trends.get,reverse=True)
        handles=sorted(handles,key=trends.get,reverse=True)

    def trends_handles_by_lang(self, lang ):
        """ get tweets, trends, most mentioned handles by lang
         function call example : object.trends_handles_by_lang('"en"')
         Input : language E.g 'en', 'ta'
         Output : A tuple with all tweets in the language, trends,handles,
        coord's
        """
        lang_tweets = []
        coordinates = []
        results = self.store.call_view("_design/tweets",
                                       "by_lang_text",
                                       options={'key': '"%s"' % str(lang)})
        lang_trends = defaultdict(int)
        lang_handles = defaultdict(int)
        if results:
            for row in results['rows']:
                sliced_value = row['value']
                tweets = sliced_value[0]
                if(sliced_value[1] != "null"):
                    coordinates.append(sliced_value[1])
                for terms in tweets:
                    if (terms.startswith('#')):
                        lang_trends[terms] += 1
                    if(terms.startswith('@')):
                        lang_handles[terms] += 1
                lang_tweets.append(tweets)
        return (lang_tweets, lang_trends, lang_handles, coordinates)

    def sentiments_of_language(self):
        """Generates a sentiment tuble by categorizing tweets by
        languages. Compares fixed languages for now.
        
        Languages to compare: en
        """
        handles_en = self.trends_handles_by_lang("en")[0]
        handles_it = self.trends_handles_by_lang("it")[0]
        handles_es = self.trends_handles_by_lang("es")[0]    
        sentiments_en = self.analyse_sentiments(handles_en)
        sentiments_it = self.analyse_sentiments(handles_it)
        sentiments_es = self.analyse_sentiments(handles_es)

        return {'rows': [{'sentiments_en': list(sentiments_en)},
                         {'sentiments_it': list(sentiments_it)},
                         {'sentiments_es': list(sentiments_es)}]}

    def analyse_sentiments(self, tweets_to_analyse):
        """Get sentiments of a particular topic.
           Input : tweets of a topic stored in an array
           Returns :tuple with sentiment count """
        senti_extremelynegative = 0
        senti_negative = 0
        senti_neutral = 0
        senti_positive = 0
        senti_extremelypositive = 0
        for s_tweet in tweets_to_analyse:
            senti_tweet = TextBlob(s_tweet)
            polarity = senti_tweet.sentiment.polarity
            if polarity < 0:
                if polarity < -0.5:
                    sentiment = "extremely negative"
                    senti_extremelynegative += 1
                else:
                    sentiment = "negative"
                    senti_negative += 1
            elif senti_tweet.sentiment.polarity == 0:
                sentiment = "neutral"
                senti_neutral += 1
            else:
                if polarity > 0.5:
                    sentiment = "extremely positive"
                    senti_extremelypositive += 1
                else:
                    sentiment = "positive"
                    senti_positive += 1

        sentiments_tup = (senti_extremelynegative, senti_negative,
                          senti_neutral, senti_positive,
                          senti_extremelypositive)

        return sentiments_tup

    def timeslots(self, term):
        """ Returns a of tuples with occurrences of a term per day
        Example input: ='"#iphone"'
        """
        results = self.store.call_view("_design/tweets",
                                       "by_coordinates_time",
                                       options={'key': '"%s"' % term})
        week_counts = [0, 0, 0, 0, 0, 0, 0]
        if results:
            for row in results['rows']:
                sliced_value = row['value']
                time_str = sliced_value[1]
                time_obj = time.strptime(time_str, "%a %b %d %H:%M:%S +0000 %Y")
                week_counts[time_obj.tm_wday] += 1

        return {'rows': [{'week_counts': week_counts}]}

    def topic_counts_last_week(self, topic):
        """Returns data on how many times a topic was mentioned on a
        particular day in the last week. That is the last week of
        project submission."""
        results = self.store.call_view("_design/tweets",
                                       "by_coordinates_time",
                                       options={'key': '"%s"' % topic})
        # make the week range
        past_week_dates = get_past_week()
        past_week_counts = [0, 0, 0, 0, 0, 0, 0]
        if results['rows']:
            for row in results['rows']:
                sliced_value = row['value']
                time_str = sliced_value[1]
                time_obj = time.strptime(time_str,
                                         "%a %b %d %H:%M:%S +0000 %Y")
                # Get the isodate representation                
                dt = datetime.datetime.fromtimestamp(mktime(time_obj))
                isodate = dt.now().date().isoformat()                
                # Get index of the count slot to increment
                index = past_week_dates.index(isodate)
                print index
                # increment the count
                past_week_counts[index] += 1

        print past_week_dates
        return past_week_counts


def get_past_week():
    """Return the full week (Sunday first) of the week containing the given date.

  'date' may be a datetime or date instance (the same type is returned).
  """
    one_day = datetime.timedelta(days=1)
    today = datetime.date.today()
    past_week = []

    for i in range(7):
        past_week.append(today.isoformat())
        today -= one_day

    return list(reversed(past_week))

##########################################################################
# Custom topic analysis                                                  #
##########################################################################

def popular_tourist_places():
    """Compare sentiments of tourist places so that we can find the most happiest
        and the least happiest tourist places.The places & hashtags were selected
        based on twitter search and some blogs"""
    return ["#goldengatebridge", "#goldengate",
            "#alcatraz", "#fishermanswharf",
            "#wharf", "#sfzoo", "#presidio"]

def popular_sport():
    """Compare sentiments of sports teams so that we can find out which team/sport
    is follwed most, The hashtags were taken after referring to team's website
    and some blogs"""
    return ["#49ers", "#cometoplay", "#niners",
            "#sfgiants", "#wearesf", "#wearegiant"]


def public_transport():
    """Compare sentiments of two popular public trasnport systems.
        The hashtags were selected based on twitter search and some blogs"""
    return ["#muni", "#sfmuni", "#bart"]



if __name__ == '__main__':
    tracker = Analyser()
    # result = tracker._get_user_ids()
    # result = tracker._tweets_by_user(740983)
    # print tracker.get_top_retweets()
    # print tracker.sentiments_of_topic("#iphone")
    # print tracker.get_sentiments_on_topics(["#muni", "#sfmuni", "#bart"])
    # print tracker.timeslots("#iphone")
    # print tracker.topic_counts_last_week("california")
    print tracker.sentiments_of_language()
