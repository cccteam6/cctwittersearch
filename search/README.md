# Important

Please use `virtualenv`. The requirements are in `../deploy/requirements.txt`.

# Server

The `tweetserver.py` is a [ Flask ](http://flask.pocoo.org/) server which is
served using [ Gunicorn ](http://gunicorn.org/). The server exposes a mini REST
api which responds to GET requests. Each GET requests makes the accumulator call
the CouchDB HTTP API on the partitioned couchdb instances and then returned the
reduced or accumulated result back to the caller (the interface).

`Gunicorn` is proxied externally through `nginx`.

# Analytical Interface

`overview.py` and `analysis.py` and `geofactory.py` implement a multitude of
methods to call views and use their results in different manners.


# Tweetstore

`tweetstore.py` is the interface to a couchdb instance using both
`python-couchdb` and `urllib2` to make raw GET/DELETE calls. It can delete
documents, get documents, call a view by name .etc. It is mainly used to
load the design document containing the views from `designdoc.json` into
the couchdb instance.


# Harvestor

`process.py` implements the harvestor through the class `TwitterAccess`. The
harvestor takes a pipe of a url file which contain the urls to the multiple
partitioned couchdb instances. The harvested tweets are split over them. If any
one of them does not accept the tweets, leftovers are generated and those tweets
are given to the next couchdb instance in line.

The twitter data is gotten through cursoring through the Twitter API results as
mentioned on the twitter guidelines. A window is created using since_id and
max_id over the returned timelined, and harvested.

To prevent duplicates, all the existing tweet_ids are loaded into a dictionary
through a view call at the start of the harvest, and the returned tweets are
continuously checked against this dictionary.

Usage:
    python process.py < localip

Where localip is a file containing the ip addresses of the couchdb instances
to harvest to. One ip per line.

# Accumulator

`accumulator.py` sits in between the analytical methods and the web server. It
implements the methods which can call the analytical methods on multiple
couchdb instances and accumulate them in different ways into one result. 
