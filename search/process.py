#!/usr/local/bin/python
# Team 23 - San Francisco - Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065), 
# Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142), 
# Rupali Patil (726 452)from tweetstore import TweetStore

# import tweepy
import twitter
import threading
import time, sys
from tweetstore import TweetStore
from apiglobals import *

import logging
import sys

root = logging.getLogger()
root.setLevel(logging.DEBUG)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(message)s')
ch.setFormatter(formatter)
root.addHandler(ch)


requests_done = 0


class TwitterAccess:
    """Class for api access to twitter and representing tweets as a
    dictionary."""
    since_id = None
    max_id = -1L
    seen_ids = {}

    def __init__(self, query):
        self.api = twitter.Api(consumer_key=CONSUMER_KEY,
                               consumer_secret=CONSUMER_SECRET,
                               access_token_key=ACCESS_TOKEN,
                               access_token_secret=ACCESS_TOKEN_SECRET)
        self.query = query

    def do_search(self, count=100):
        """Performs a search query on the tweepy api object to get
        a maximum of 100 tweets per request."""
        since_id = TwitterAccess.since_id
        max_id = TwitterAccess.max_id
        returned_count = 0

        while returned_count <= count:
            try:
                if max_id <= 0:
                    if not since_id:
                        new_tweets = self.api.GetSearch(term=self.query,
                                                        count=count)
                    else:
                        new_tweets = self.api.GetSearch(term=self.query,
                                                        count=count,
                                                        since_id=since_id)
                else:
                    if not since_id:
                        new_tweets = self.api.GetSearch(term=self.query,
                                                        count=count,
                                                        max_id=str(max_id - 1))
                    else:
                        new_tweets = self.api.GetSearch(term=self.query,
                                                        count=count,
                                                        max_id=str(max_id - 1),
                                                        since_id=since_id)
                if not new_tweets:
                    logging.info("No Tweets Found.")
                    return []
                returned_count += len(new_tweets)
                max_id = new_tweets[-1].id
            except Exception as e:
                logging.info("Error: ", str(e))
                return []

        TwitterAccess.max_id = max_id
        return new_tweets

    def make_tweets_dict(self, tweets):
        dict_tweets = []
        for tweet in tweets:
            tweet_id = str(tweet.GetId())
            if str(tweet_id) not in TwitterAccess.seen_ids:
                TwitterAccess.seen_ids[tweet_id] = 1
                # Convert the tweet object to a dictionary
                dict_t = tweet.AsDict()
                # Instead of storing the id as the default int, store it
                # as a string so that javascript does not cut off the
                # integer.
                dict_t['id'] = tweet_id
                dict_tweets.append(dict_t)
        return dict_tweets

    def load_seen_ids(self, tweetstores):
        """Populates the dictionary of seen ids from the list of tweetstores.
        ids."""
        counter = 0
        # Call by_ids view on the tweetstore to get tweet_ids
        for tweetstore in tweetstores:
            try:
                result = tweetstore.call_view("_design/tweets", "by_ids")
                if result['rows']:
                    for row in result['rows']:
                        counter += 1    # Tracking how many ids loaded
                        TwitterAccess.seen_ids[str(row['key'])] = 1
                else:
                    logging.info("ERROR: calling view: " % result)
            except:
                # Could not get ids from the store
                # must be down
                # let's gracefully continue
                logging.info("Could not load ids from %s" % tweetstore.dburl)

        logging.info("%d ids were loaded." % (len(TwitterAccess.seen_ids), ))


def flush_seen():
    """Flush the seen ids to not fill up the memory."""
    global seen_ids
    seen_ids = {}


def interval_search(accessor, tweetstores):
    """This function is to run periodically to get as many tweets as it
    can and then store it to the tweetstore."""

    logging.info("Searching for tweets...")
    global requests_done   # Count the number of requests

    # Search
    fresh_tweets = accessor.do_search()
    tweets_dict = accessor.make_tweets_dict(fresh_tweets)
    tweets_returned_n = len(tweets_dict)
    leftovers = []    # Hold tweets which could not be stored on a store
    # If tweets were found, run the search again for more until
    # none are found
    while tweets_returned_n > 0:
        # Split the tweets_dict amongst the tweetstores as equally as possible
        # Last server probably gets more, a little more
        cover = tweets_returned_n / len(tweetstores)
        for i, store in enumerate(tweetstores):
            split = []   # The split
            if i == len(tweetstores) - 1:
                # Spread till the end
                split = tweets_dict[(i*cover):]
            else:
                split = tweets_dict[(i*cover):(i*cover) + cover]
                # Empty leftovers into split
            split.extend(leftovers)
            leftovers = []
            try:
                store.store_tweets(split)
                logging.info("<R: %d> Stored %d tweets on %s" % (requests_done,
                                                                 len(split),
                                                                 store.dburl))
            except:
                logging.info("%s is down. Leftovers generated." % store.dburl)
                leftovers.extend(split)

        requests_done += 1
        fresh_tweets = accessor.do_search()
        tweets_dict = accessor.make_tweets_dict(fresh_tweets)
        tweets_returned_n = len(tweets_dict)

    # Set the since_id to the last max_id
    # so that the search resets next time
    if tweets_returned_n == 0:
        logging.info("No more fresh tweets.")
        TwitterAccess.since_id = TwitterAccess.max_id
        TwitterAccess.max_id = -1L

    logging.info("Waiting for 180 seconds.")
    # Sleep for 180 s
    time.sleep(180)
    # Search again
    interval_search(accessor, tweetstores)


if __name__ == '__main__':
    # Get the couchdb server ip addresses
    dbips = []
    logging.info("Reading IP file...")
    for line in sys.stdin:
        dbips.append("http://" + line.strip() + ":5984/")

    logging.info("Couchdb IPs: %s" % dbips)
    
    # Get query parameter from the command line arguments
    if len(sys.argv) > 1:
        query = sys.argv[1]
    else:
        query = sample_query_1

    logging.info("Querying search api with %s." % query)

    # Create tweetstore objects on the ip addresses
    tweetstores = []
    for ip in dbips:
        logging.info("Creating server connection to %s" % ip)
        try:
            store = TweetStore(DBNAME, ip)
            tweetstores.append(store)
        except:
            # Could not create a store connection to the couchdb
            logging.info("Could not create a connection to %s" % ip)
            continue
    print tweetstores
    accessor = TwitterAccess(query)

    # Load the last seens and then start the search
    accessor.load_seen_ids(tweetstores)
    interval_search(accessor, tweetstores)

    logging.info("FIN")
