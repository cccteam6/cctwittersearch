activate_this = '/mnt/data/cctwittersearch/venv/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

import sys
sys.path.insert(0, '/mnt/data/cctwittersearch/search')

from tweetserver import app as application
