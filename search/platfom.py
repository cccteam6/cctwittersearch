# how many tweeter are using which platform 
# Team 23 - San Francisco - Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065), 
# Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142), 
# Rupali Patil (726 452)from tweetstore import TweetStore

from tweetstore import TweetStore
from apiglobals import DBNAME


class access_device:
    """Methods which pull tweet and user coordinates from the coucbDB."""

    def __init__(self, dbname=DBNAME):
        self.store = TweetStore(dbname)

  
    def platform(self):
        """Get the number of user accessing from devices like android,iOS,mac,web,tweet deck,other"""
        result = self.store.call_view('_design/tweets',
                                      "by_platform")
        if result:
            return result['rows']
        return None


if __name__ == '__main__':
    p = access_device()
        print p.platform()
