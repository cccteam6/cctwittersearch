# Team 23 - San Francisco - Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065), 
# Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142), 
# Rupali Patil (726 452)from tweetstore import TweetStore

from tweetstore import TweetStore
from overview import Overview
from geofactory import GeoFactory

from analysis import Analyser, public_transport, popular_sport, popular_tourist_places


from apiglobals import DBNAME

from collections import defaultdict


class Accumulator:
    """Implments methods which encapsulate a list of tweetstores and call
    the analysis methods over each of them and accumulates the individual
    results into one result for the server to reply with."""

    def __init__(self, dbname=DBNAME, store_urls=None):
        """stores: list of tweetstore urls"""
        # Default
        if not store_urls:
            store_urls = ["localhost"]

        self.tweetstores = []
        # Create the stores
        for url in store_urls:
            dburl = "http://" + url.strip() + ":5984/"
            print "Creating store for ", dburl
            store = TweetStore(DBNAME, dburl=dburl)
            self.tweetstores.append(store)

    def get_result(self, target, method, args=[], kwargs={}):
        """Simply get results from the method on target on the different stores
        and return them."""
        combined_result = []
        for store in self.tweetstores:
            print "Getting result from ", store.dburl
            # Set the target's store to our store
            target.store = store
            methodToCall = getattr(target, method)
            result = methodToCall(*args, **kwargs)
            combined_result.append(result)

        return combined_result

    def accumulate(self, target, method, args=[], kwargs={}):
        """Initializes the analysis target object with each of the instance
        stores and then call the method on that.

        The method will call the view and return a result. All these indvidual
        results have to be combined and returned.

        'method' is the string name of the method on target
        """
        combined_result = []
        for store in self.tweetstores:
            print "Getting result from ", store.dburl
            # Set the target's store to our store
            target.store = store
            methodToCall = getattr(target, method)
            result = methodToCall(*args, **kwargs)
            if result['rows']:
                print "Got %d rows from %s" % (len(result['rows']),
                                               store.dburl)
                combined_result.extend(result['rows'])
            else:
                print "Got no valid rows from %s" % store.dburl

        return {'rows': combined_result}

    def sum_counts(self, result):
        rows = result['rows']
        combined_dict = defaultdict(int)
        counted_result = []

        for row in rows:
            combined_dict[row['key']] += row['value']

        # Re wrap into the usual result representation
        for key in combined_dict:
            counted_result.append({'key': key,
                                   'value': combined_dict[key]})
        return {'rows': counted_result}

    def combine_by_key(self, result, key):
        """Custom combination by key in result['rows']"""
        rows = result['rows']
        # rows is a list of dicts, focusing on the 'key', key of the dict
        combined_list = []
        for d in rows:
            if key in d:
                list_to_combine = d[key]
            else:
                continue
            if combined_list == []:
                combined_list = list_to_combine
            else:
                combined_list = [x + y for x, y in
                                 zip(combined_list, list_to_combine)]
        return {'rows': [{key: combined_list}]}

    def combine_by_keys(self, result, keys):
        """Combines by 'combine_by_key' on key of 'keys' and then re zips
        them into a rows dictionary."""
        complete_combined = {'rows': []}
        for key in keys:
            combined = self.combine_by_key(result, key)
            complete_combined['rows'].append(combined['rows'][0])
        return complete_combined

    def sort_by_key(self, result, key):
        """Sort the rows again by the given key. Descending."""
        list_to_sort = result['rows']
        sorted_list = sorted(list_to_sort, key=lambda x: x[key], reverse=True)
        return {'rows': sorted_list}


def load_urls(urlfile):
    """Load the urls from a file."""
    urls = []
    with open(urlfile) as fo:
        for line in fo:
            urls.append(line.strip())
    return urls


if __name__ == '__main__':
    over = Overview()
    geo = GeoFactory()
    any = Analyser()
    urls = load_urls("localip")
    print urls
    acc = Accumulator(store_urls=urls)

    result = acc.get_result(any, 'get_tweets_by_topics', ['california'])
    print result
    # summed = acc.combine_by_keys(result, ['sentiments_en',
    #                                       'sentiments_it',
    #                                       'sentiments_es'])
    # print summed
