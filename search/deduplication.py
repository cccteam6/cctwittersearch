# Team 23 - San Francisco - Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065), 
# Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142), 
# Rupali Patil (726 452)from tweetstore import TweetStore

from tweetstore import TweetStore
from apiglobals import *
import logging, sys

root = logging.getLogger()
root.setLevel(logging.DEBUG)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(message)s')
ch.setFormatter(formatter)
root.addHandler(ch)


def get_duplicate_ids(store):
    """Calls a view on tweetstore to get duplicate tweet_ids"""
    duplicate_ids = []
    result = store.call_view("_design/tweets", "by_unique_ids",
                             options={'group': 'true'})
    if result['rows']:
        # Select all the rows which have a value greater than 1 (dups)
        for row in result['rows']:
            if int(row['value']) > 1:
                duplicate_ids.append(row['key'])
    else:
        logging.info("Returned Result: %s" % result)
        return result
    return duplicate_ids


def delete_tweet_id(store, duplicate_ids):
    """Deletes the extra couchdb document for each duplicate_ids."""
    for tweet_id in duplicate_ids:
        result = store.call_view("_design/tweets", "by_ids",
                                 options={'key': str(tweet_id)})
        if result['rows']:
            rows = result['rows']
            # Check if there is indeed more than one row/duplicate
            if len(rows) > 1:
                # Delete all but the last one
                for delete_row in rows[1:]:
                    # Get the revision id
                    doc = store.get_doc(delete_row['id'])
                    revision = doc['_rev']
                    # Call HTTP DELETE on latest revision
                    store.delete_doc(delete_row['id'], revision)


if __name__ == '__main__':
    # Get the couchdb server ip addresses
    dbips = []
    for line in sys.stdin:
        dbips.append("http://" + line.strip() + ":5984/")

    # For no input ip file, the one url is localhost
    if dbips == []:
        dbips.append("http://localhost:5984/")

    logging.info("Couchdb IPs: %s" % dbips)

    for url in dbips:
        try:
            store = TweetStore(DBNAME, url)
            # Get list of duplicate tweet_ids
            dups = get_duplicate_ids(store)
            delete_tweet_id(store, dups)
        except:
            # Could not create a store connection to the couchdb
            logging.info("Could not create a connection to %s" % url)
            continue
