# Team 23 - San Francisco - Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065), 
# Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142), 
# Rupali Patil (726 452)from tweetstore import TweetStore

from tweetstore import TweetStore
from apiglobals import DBNAME


class GeoFactory:
    """Methods which pull tweet and user coordinates from the coucbDB."""

    def __init__(self, dbname=DBNAME):
        self.store = TweetStore(dbname)

    def tweets_by_coordinates(self):
        """Get the list of tweet_ids and coordinates."""
        result = self.store.call_view('_design/tweets', 'by_coordinates',
                                      options={'group': 'true'})
        if result:
            return result
        return {}

    def tweets_coordinates_of_user(self, user_id):
        """Get the coordinates of the tweets written by the user_id"""
        result = self.store.call_view('_design/tweets',
                                      "by_user_tweet_coordinates",
                                      options={'key': user_id})
        if result:
            return result
        return {}


if __name__ == '__main__':
    geo = GeoFactory()
    # print geo.tweets_by_coordinates()
    print geo.tweets_coordinates_of_user(60452453)
