#!/usr/local/bin/python
# Team 23 - San Francisco - Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065), 
# Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142), 
# Rupali Patil (726 452)from tweetstore import TweetStore

import couchdb
import json
from apiglobals import DBNAME

import urllib2, httplib
from urllib import urlencode
from urlparse import urlparse
from urlparse import urljoin


class TweetStore:
    """CouchDB storage for tweets."""

    def __init__(self, dbname, dburl="http://localhost:5984/"):
        self.dbname = dbname
        self.dburl = dburl
        self.server_connect()

    def server_connect(self):
        """Create a connection to the couchDB DB."""
        try:
            self.server = couchdb.Server(url=self.dburl)
            self.db = self.server.create(self.dbname)
        except couchdb.http.PreconditionFailed:
            # Get the existing db
            self.db = self.server[self.dbname]

    def get_doc(self, doc_id):
        """Get the whole tweet data for the document with doc_id."""
        try:
            doc = self.db[doc_id]
            if not doc:
                return None
            return doc
        except couchdb.http.ResourceNotFound as e:
            print e
            return None

    def get_tweet(self, tweet_id):
        """Get a tweet by the actual tweet id."""
        result = self.call_view("_design/tweets", "by_ids",
                                {'key': str(tweet_id)})
        if result['rows']:
            doc_id = result['rows'][0]['value']
            doc = self.get_doc(doc_id)
            if doc:
                return doc
            else:
                return None
        return None

    def store_tweet(self, tweet):
        """Store the tweet dict as a document in couchdb"""
        self.db.save(tweet)

    def store_tweets(self, tweets):
        for tweet in tweets:
            self.db.save(tweet)

    def _load_design_json(self, jsonfile):
        """Loads json from a file representing a design document
        and converts it into a python dictionary."""
        with open(jsonfile) as jf:
            jsontext = jf.read()
            # Decode
            return json.loads(jsontext)

    def store_designs(self):
        """Store a design documents containing the views."""
        doc = self._load_design_json("designdoc.json")
        # Check if there is an existing design doc already
        if doc["_id"] in self.db:
            olddoc = self.db[doc["_id"]]
            # If there is then assign it the previous revision id
            if "_rev" in olddoc:
                doc["_rev"] = olddoc["_rev"]
        print "Storing design document..."
        return self.db.save(doc)

    def call_view(self, design_doc_name, view_name, options={}):
        """Calls an existing view on the couchDB instance and returns the
        data as a python dictionary.

        Currently the views are stored in the class variable _views.
        Uses urllib2"""
        # Construct the query url
        query_url = "%s/%s/_view/%s" % (urljoin(self.dburl, self.dbname),
                                        design_doc_name,
                                        view_name)
        # Encode the options
        if options:
            query_url += "?" + urlencode(options)
        print "Calling: %s" % (query_url, )
        # Run A GET request
        try:
            response = urllib2.urlopen(query_url)
            return json.loads(response.read())
        except urllib2.HTTPError as e:
            print e
            return None
        except Exception as e:
            print e
            return None

    def delete_doc(self, doc_id, revision):
        """Calls a HTTP DELETE to delete the document from couchdb on
        its latest revision."""        
        query_url = "%s/%s" % (urljoin(self.dburl, self.dbname),
                               str(doc_id))
        print "Calling: DELETE %s" % (query_url, )
        
        # Calling DELETE through httplib connection
        # First split the url into hostname and port again
        parsed = urlparse(self.dburl)
        conn = httplib.HTTPConnection(parsed.hostname, int(parsed.port))
        conn.request('DELETE', '/%s/%s?rev=%s' % (self.dbname, str(doc_id),
                                                  str(revision)))
        resp = conn.getresponse()        
        return resp.read()

if __name__ == '__main__':
    tweetstore = TweetStore(DBNAME)
    print tweetstore.store_designs()
    # result = tweetstore.call_view("_design/tests", "by_lang",
    #                               {'group': 'true', 'group_level': 1})

