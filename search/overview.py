# Team 23 - San Francisco - Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065), 
# Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142), 
# Rupali Patil (726 452)from tweetstore import TweetStore

from tweetstore import TweetStore
from apiglobals import DBNAME


class Overview:
    '''Incorporates different view calls on the couchDB into meaningful
    results.'''

    def __init__(self, dbname=DBNAME):
        self.store = TweetStore(dbname)

    def get_top_retweets(self, n=10):
        """Generate tweets which have been retweeted the most.

        Returns a json array response to be used in REST response.
        """
        response_json = []   # Sorted tweets by retweets as a json array
        results = self.store.call_view("_design/tweets",
                                       "by_retweet_counts")
        if results:
            # Considering only > 1 retweets as essential rows
            essential_rows = filter(lambda x: int(x['value']) > 1,
                                    results['rows'])
            sorted_tweets = sorted(essential_rows,
                                   key=lambda x: x['value'],
                                   reverse=True)[:n]
            # Getting the text of the top retweeted tweets
            for tweet in sorted_tweets:
                doc = self.store.get_doc(tweet['id'])
                if doc:
                    tweet['text'] = doc['text']
                    response_json.append(tweet)
        return {'rows': response_json}

    def get_top_languages(self):
        '''Get the top languages used in the couchDB store.'''
        result = self.store.call_view("_design/tweets", "by_lang",
                                      {'group': 'true', 'group_level': 1})

        if result:
            return result
        return None

    def get_all_tweets(self):
        """Get all the tweets by their ids."""
        result = self.store.call_view("_design/tweets", "by_ids")
        if result:
            return result['rows']
        return None

    def most_followed(self, n=10):
        """Get the n most followed users in the db"""
        # Call view
        results = self.store.call_view("_design/tweets", "count_followers")

        if results['rows']:
            # Sort reverse and get the top 10 follow counts
            sorted_counts = sorted(results['rows'],
                                   key=lambda x: x['value'],
                                   reverse=True)[:n]
            return {'rows': sorted_counts}
        return {}

    def most_tweets_by_user(self, n=10):
        """Get the n top tweeters in the db"""
        # Call view
        results = self.store.call_view("_design/tweets", "most_tweets",
                                       options={'group': 'true'})

        if results['rows']:
            rows = results['rows']
            # filter users with counts below a threshold
            essential_rows = filter(lambda x: x['value'] > 10, rows)
            # Sort reverse and get the top 10 tweet counds
            sorted_counts = sorted(essential_rows,
                                   key=lambda x: x['value'],
                                   reverse=True)[:n]
            return {'rows': sorted_counts}
        return {}

    def most_mentioned(self, n=10):
        """Get the n most followed users in the db"""
        # Call view
        results = self.store.call_view("_design/tweets", "most_mentioned",
                                       options={'group': 'true'})        
        if results['rows']:
            # Sort reverse and get the top 10 follow counts
            sorted_counts = sorted(results['rows'],
                                   key=lambda x: x['value'],
                                   reverse=True)[:n]
            return {'rows': sorted_counts}
        return {}

    def userinfo_by_id(self, user_id):
        """Get the screen_name of the user with the id user_id"""
        result = self.store.call_view("_design/tweets", "by_user_ids",
                                      options={'key': int(user_id),
                                               'limit': 1})
        if result:
            if result['rows']:
                row = result['rows'][0]
                document_id = row['id']
                doc = self.store.get_doc(document_id)
                return doc['user']
            else:
                return None
        else:
            return None

    def userinfo_by_username(self, user_name):
        """Get the screen name of user_id"""
        result = self.store.call_view("_design/tweets", "username_to_userid",
                                      options={'key': '"%s"' % user_name,
                                               'limit': 1})
        if result:
            if result['rows']:
                row = result['rows'][0]
                document_id = row['id']
                doc = self.store.get_doc(document_id)
                return doc['user']
            else:
                return None
        else:
            return None


if __name__ == '__main__':
    tracker = Overview()
    # result = tracker._get_user_ids()
    # result = tracker._tweets_by_user(740983)
    # print tracker.get_top_retweets()
    # print tracker.get_top_languages()
    # print tracker.most_followed()
    print tracker.userinfo_by_id(989)
