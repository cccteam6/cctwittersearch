# Team 23 - San Francisco - Anirudh Joshi (583 426), Ashutosh Rishi Ranjan (709 065),
# Shyamsundar Dorairaj (714 199), Arvind Hariharan Sankara Subramanian (689 142),
# Rupali Patil (726 452)from tweetstore import TweetStore

from flask import Flask, jsonify, Response, redirect
import json

from overview import Overview
from geofactory import GeoFactory
from tweetstore import TweetStore
import analysis
from accumulator import Accumulator
from apiglobals import DBNAME

dbips = []
# Load the ips
with open("../deploy/processor_ip_addresses") as fo:
    for line in fo:
        dbips.append(line.strip())
print "Ips loaded ", dbips

app = Flask(__name__, static_url_path='')

tracker = Overview()
geo = GeoFactory()
analyser = analysis.Analyser()

acc = Accumulator(DBNAME, store_urls=dbips)


@app.route("/")
def index():
    return redirect("index.html", code=302)


@app.route("/user/get_info_by_id/<user_id>")
def get_user_info_by_id(user_id):
    infos = acc.get_result(tracker, 'userinfo_by_id', [user_id])
    # There may be None values in the infos array, find the first
    # non None
    user_info = [a for a in infos if a is not None][0]
    resp = Response(json.dumps(user_info))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Content-Type'] = 'application/json'
    return resp


@app.route("/user/get_info_by_name/<user_name>")
def get_user_info_by_username(user_name):
    infos = acc.get_result(tracker, 'userinfo_by_username', [user_name])
    # There may be None values in the infos array, find the first
    # non None
    user_info = [a for a in infos if a is not None][0]
    resp = Response(json.dumps(user_info))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Content-Type'] = 'application/json'
    return resp



@app.route("/tweets/topic/<topic>")
def tweets_on_topic(topic):
    result = acc.get_result(analyser, 'get_tweets_by_topics', [topic])
    resp = Response(json.dumps(result))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Content-Type'] = 'application/json'
    return resp


@app.route("/tweet/<tweet_id>")
def show_tweet(tweet_id):
    return json.dumps(tracker.store.get_tweet(tweet_id))


@app.route("/tweets/top_retweets")
def top_retweets():
    top_retweets = tracker.get_top_retweets()
    resp = Response(json.dumps(top_retweets))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Content-Type'] = 'application/json'
    return resp


@app.route("/tweets/top_languages", methods=['GET'])
def top_languages():
    result = acc.accumulate(tracker, 'get_top_languages')
    summed_result = acc.sum_counts(result)
    resp = Response(json.dumps(summed_result['rows']))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Content-Type'] = 'application/json'
    return resp


# Most followed user
@app.route("/users/most_followed", methods=["GET"])
def most_followed():
    result = acc.accumulate(tracker, 'most_followed')
    summed_result = acc.sum_counts(result)
    sorted_result = acc.sort_by_key(summed_result, 'value')
    resp = Response(json.dumps(sorted_result['rows']))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Content-Type'] = 'application/json'
    return resp


# Top tweeter
@app.route("/users/top_tweeter", methods=['GET'])
def top_tweeters():
    result = acc.accumulate(tracker, 'most_tweets_by_user')
    summed_result = acc.sum_counts(result)
    sorted_result = acc.sort_by_key(summed_result, 'value')
    resp = Response(json.dumps(sorted_result['rows']))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Content-Type'] = 'application/json'
    return resp

# Top mentioned user
@app.route("/users/most_mentioned", methods=['GET'])
def most_mentioned():
    result = acc.accumulate(tracker, 'most_mentioned')
    summed_result = acc.sum_counts(result)
    sorted_result = acc.sort_by_key(summed_result, 'value')
    resp = Response(json.dumps(sorted_result['rows']))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Content-Type'] = 'application/json'
    return resp


##########################################################################
# Geo factory API functions                                              #
##########################################################################

@app.route("/geo/all_coordinates", methods=['GET'])
def all_coordinates():
    result = acc.accumulate(geo, 'tweets_by_coordinates')
    resp = Response(json.dumps(result['rows']))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Content-Type'] = 'application/json'
    return resp


@app.route("/geo/user/<user_id>", methods=['GET'])
def user_coordinates(user_id):
    result = acc.accumulate(geo, 'tweets_coordinates_of_user',
                            [user_id])
    resp = Response(json.dumps(result['rows']))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Content-Type'] = 'application/json'
    return resp


##########################################################################
# Sentiment Analysis                                                     #
##########################################################################

@app.route("/sent/topic/<topic>")
def analyse_topic(topic):
    sentiments = acc.accumulate(analyser, 'sentiments_of_topic', [topic])
    combined = acc.combine_by_key(sentiments, 'sentiments')
    resp = Response(json.dumps(combined))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Content-Type'] = 'application/json'
    return resp


# Preset topics
# For sport topics
@app.route("/sent/preset/sports")
def anayse_sports():
    """Use the preset topics."""
    topics_list = analysis.popular_sport()
    result = acc.accumulate(analyser, 'multiple_sentiments',
                            [topics_list])
    combined = acc.combine_by_keys(result,
                                   map(analyser._make_sentiment_key,
                                       topics_list))
    print combined
    # Add the topic list to the json as well
    combined['topics'] = topics_list
    resp = Response(json.dumps(combined))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Content-Type'] = 'application/json'
    return resp


# for tourist places
@app.route("/sent/preset/tourist_places")
def anayse_tourist_places():
    """Use the preset topics."""
    topics_list = analysis.popular_tourist_places()
    result = acc.accumulate(analyser, 'multiple_sentiments',
                            [topics_list])
    combined = acc.combine_by_keys(result,
                                   map(analyser._make_sentiment_key,
                                       topics_list))
    print combined
    # Add the topic list to the json as well
    combined['topics'] = topics_list
    resp = Response(json.dumps(combined))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Content-Type'] = 'application/json'
    return resp


# for public transport
@app.route("/sent/preset/transport")
def analyse_transport():
    """Use the preset topics."""
    topics_list = analysis.public_transport()
    result = acc.accumulate(analyser, 'multiple_sentiments',
                            [topics_list])
    combined = acc.combine_by_keys(result,
                                   map(analyser._make_sentiment_key,
                                       topics_list))
    print combined
    # Add the topic list to the json as well
    combined['topics'] = topics_list
    resp = Response(json.dumps(combined))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Content-Type'] = 'application/json'
    return resp


# week count of a topic
@app.route("/sent/weekcount/<topic>")
def weekcount(topic):
    result = acc.accumulate(analyser, 'timeslots',
                            [topic])
    combined = acc.combine_by_key(result, 'week_counts')
    resp = Response(json.dumps(combined))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Content-Type'] = 'application/json'
    return resp


# Sentiments by languages
@app.route("/sent/languages_sentiments")
def languages_sentiments():
    result = acc.accumulate(analyser, 'sentiments_of_language')
    combined = acc.combine_by_keys(result, ['sentiments_en',
                                            'sentiments_it',
                                            'sentiments_es'])
    resp = Response(json.dumps(combined))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Content-Type'] = 'application/json'
    return resp


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0', port=8080)
